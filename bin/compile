#!/usr/bin/env bash
# bin/compile <build-dir> <cache-dir>

# fail fast
set -e

# debug
# set -x

# parse and derive params
BUILD_DIR=$1
CACHE_DIR=$2
LP_DIR=`cd $(dirname $0); cd ..; pwd`

function error() {
  echo " !     $*" >&2
  exit 1
}

function topic() {
  echo "-----> $*"
}

function indent() {
  c='s/^/       /'
  case $(uname) in
    Darwin) sed -l "$c";;
    *)      sed -u "$c";;
  esac
}

APT_CACHE_DIR="$CACHE_DIR/apt/cache"
APT_STATE_DIR="$CACHE_DIR/apt/state"

mkdir -p "$APT_CACHE_DIR/archives/partial"
mkdir -p "$APT_STATE_DIR/lists/partial"

APT_OPTIONS="-o debug::nolocking=true -o dir::cache=$APT_CACHE_DIR -o dir::state=$APT_STATE_DIR"

topic "Updating apt caches"
apt-get $APT_OPTIONS update | indent

topic "Fetching .debs for libcap-dev"
apt-get $APT_OPTIONS -y --force-yes -d install --reinstall libcap-dev | indent

mkdir -p $BUILD_DIR/.apt

for DEB in $(ls -1 $APT_CACHE_DIR/archives/*.deb); do
  topic "Installing $(basename $DEB)"
  dpkg -x $DEB $BUILD_DIR/.apt/
done

mkdir -p $BUILD_DIR/vendor/lib
mkdir -p $BUILD_DIR/vendor/include

topic "copying include and lib to vendor"
cp -r $BUILD_DIR/.apt/lib/* $BUILD_DIR/vendor/lib
cp -r $BUILD_DIR/.apt/lib/x86_64-linux-gnu/* $BUILD_DIR/vendor/lib
cp -r $BUILD_DIR/.apt/usr/include/* $BUILD_DIR/vendor/include
